

/*
in index.html, link script.js to the file

in script.js,
create a function that takes 4 strings: name,email,job,hobby and it should return the following output:

Hi, my name is <name>, and my email address is <email>.
I work as a <job>, and my hobbies include <hobby>

The function name is greeter.
5. To test 
 console.log(greeter("John", "john@mail.com", "waiter","singing"));
 console.log(greeter("Monika","monika@mail.com","writer", "playing piano"));
*/


function greeter(name,email,job,hobby)
{
  return ("Hi, my name is")+" " + name +" "+("and my email address is")+" " + email + " " + ("I work as a")+" "+ job + (",and my hobbies include")+" "+hobby;
}


console.log(greeter("John", "john@mail.com", "waiter","singing"));
 console.log(greeter("Monika","monika@mail.com","writer", "playing piano"));



